#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# aliases

alias ll='ls -la --color=auto'

alias ls='ls --color=auto'

alias dotgit='git --git-dir=/home/karsuren/.dotgit --work-tree=/home/karsuren'

# prompt

PS1='[\u@\h \W]\$ '

# adding directories to PATH

export PATH=/home/karsuren/Android/Sdk/platform-tools:$PATH
