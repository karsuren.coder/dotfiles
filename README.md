# What is this?

Repo for maintaing dotfiles and config files that reside in the file tree rooted at the user's home directory 

System level config files are not not managed through this repo

# Setup instructions

```
git clone --bare https://gitlab.com/karsuren.coder/dotfiles <home_dir>/.dotgit
git --git-dir <home_dir>/.dotgit --work-tree <home_dir> reset HEAD
git --git-dir <home_dir>/.dotgit --work-tree <home_dir> restore <home_dir>
git --git-dir <home_dir>/.dotgit --work-tree <home_dir> config --local status.showUntrackedFiles no
```

This will setup all the config files in the right location

The `.bashrc` file that gets configured as a result already contains an alias `dotgit` which can be used in for managing this repo from any directory without having to specify the `--git-dir` and `work-tree` options

# dotgit command sample usages

### Add a new config file to the repo

Let's say you are going to add `~/.zshrc` to the repo for the first time

First, create the `~/.zshrc` file and make the necessary changes. Then:

```
dotgit add ~/.zshrc
dotgit commit -m '<commit_message>`
dotgit push origin main
```

### Update an existing config

Let's say you are already maintaing `~/.bashrc` using the repo and now you add some updates

First, make the necessary changes to `~/.bashrc`. Then:

```
dotgit add ~/.bashrc
dotgit commit -m '<Commit_message>'
dotgit push origin main
```

In other words, you can `dotgit` command just like git as if the repo is in your current directory - except you can run it from anywhere 
