#
# ~/.bash_profile
#

# Run ~/.bashrc file on init

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Setup volume

if [ -f /usr/bin/alsamixer ] 
then
	amixer sset Master unmute
	amixer sset Master 100%
fi

# start openbox

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]
then
	exec startx
fi
